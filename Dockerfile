FROM ubuntu:18.04

# install docker

RUN apt-get -y update && apt-get -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
RUN apt-get -y update && apt-get -y install docker-ce=5:19.03.0~3-0~ubuntu-bionic docker-ce-cli=5:19.03.0~3-0~ubuntu-bionic containerd.io

# install docker-compose

RUN curl -L "https://github.com/docker/compose/releases/download/1.25.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
RUN chmod +x /usr/bin/docker-compose
